# metroconvert CLI

> A tool for converting CSVs to metro2 format.

## Installation

> TBD

## Usage
```bash
metroconvert <filepath-to-process> <type-of-file> <flags>...
```

### Available Arguments
The CLI is called with exactly two available arguments:
- `<filepath-to-process>` - _required_, this can be either a file path (must be a csv) or a directory path. See below in the Assumptions Section for more information on what directory structure is expected.
- `<type-of-file>` - _optional_, theis argument only accepts two specific values: `general` and `rental`. If provided, the tool with process every file encountered as that specific file type. The default behavior is to detect file types and process as determined. Assumptions made for this feature are below. 

### Available Flags
- `--verbose, -v ` - runs the tool in verbose mode (debug logging level enabled).
- `--output <string>, -o <string>` - _optional_, specifies a directory for metro2 files to be written out to. If not provided, metro2 files will be output to the `output/` directory as outlined below.
- `--write-exceptions, -w` - _**optional, but recommended**_, ensures the tool should write exceptions to a file (`exceptions_<csv_name>.txt`) in the same directory alongside the source file.
## How `metroconvert` works
The tool makes the following assumptions before even beginning to process files:
- Directory Structure:
 ```
data-folder/
    client-settings.csv
    <csv1-name-to-process>.csv
    exceptions_<csv1-name-to-process>.txt (if encountered)
    <csv2-name-to-process>.csv
    exceptions_<csv2-name-to-process>.txt (if encountered)
    output/
        <csv1-metro-2-file>.txt (if validation passes)
        <csv2-metro2-file>.txt (if validation passes)
 ```
The directory provided to the CLI can contain a single client or multiple clients.
- When using the file type detection feature, it is assumed that ALL rental CSVs will be prefixed with`RENTAL_`.
- The CSVs provided will follow the format of the datalinx specifications **EXACTLY**. Any exceptions found from validation of the CSVs will be output to `stderr` unless otherwise speficied with the `--write-exceptions` flag.
- The CSVs are assumed to have the exact amount of columns as specified in file specifications provided. Any extraneous columns are skipped over.
- Extra validation done for data quality will also be output to `stderr` unless otherwise specified with the `--write-exceptions` flag.
- Validation is completed on a per-file, per-record basis, files that pass validation will have a metro2 file generated for them.
- Once validation completes successfully, metro2 files will be generated in the default `output/` directory in the current folder, or to the `--output` directory defined.

## Client Settings
The only file that you are responsible to maintain is the `client-settings.csv` file. This contains the mappings from client names to the various bureau IDs required to generate a metro2 file. If an ID is not populated for a given client, metro2 generation will be skipped for that bureau. The following is the current format of the file:

```csv
client,report name,equifax,experian,transunion,address,phone_number,experian header
Intercontinental Management,,1234567890,2222222222222,444444444444444,123 Easy Street,8885541234,
...
```

### Client Settings Fields Explained
- client - This is the field that the look up is performed on. This field should be equivalent to the name provided in the file name after uppercasing all letters and replacing spaces with underscores.
- report name - This field is utilized for experian clients specifically. This field is the Reporter name that goes at the top of the file.
- equifax - equifax ID for field 5.
- experian - experian ID for field 5.
- transunion - transunion ID for field 5.
- address - required for experian clients, used in header.
- phone_number -required for experian clients, used in header.
- experian header - required for experian clients, used in header for the identification number.

## Planned Features
- **Implementation of `upload-target` flag** - this will allow direct upload of the metro2 files to an API endpoint.