package main

import (
	"context"
	"data-furnishing-tooling/pkg/cli"
)

func main() {
	root := cli.NewRoot()
	_ = root.ExecuteContext(context.Background())
}
