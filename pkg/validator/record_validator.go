package validator

import (
	"data-furnishing-tooling/pkg/common"
	"regexp"
	"time"
)

var (
	generationCodeRegExp = regexp.MustCompile(` +(JR|SR|I|II|III|IV|V|VI|VII|VIII|IX|X)`)
)

type ECOACheck struct {
	ECOACode   *string
	J2ECOACode *string
}

func (e *ECOACheck) Validate() error {
	if e.J2ECOACode == nil {
		return nil
	}
	if e.ECOACode != e.J2ECOACode {
		return &common.RecordValidationError{
			ValidationType: "ECOA Check",
			ValidationErr:  "If supplied, ECOACodes must equal",
		}
	}

	return nil
}

type DateOpenCheck struct {
	DateOpen *time.Time
}

func (d *DateOpenCheck) Validate() error {
	if d.DateOpen.IsZero() {
		return &common.RecordValidationError{
			ValidationType: "DateOpen",
			ValidationErr:  "DateOpen is a required field. Currently is either not supplied, or is a zero value.",
			Value:          d.DateOpen,
		}
	}

	return nil
}

type UpdateChargeOffAmount struct {
	OriginalChargeOffAmt *common.Amount
	AccountStatus        *string
	AmountPastDue        *common.Amount
}

func (u *UpdateChargeOffAmount) Validate() error {
	if u.OriginalChargeOffAmt == nil {
		return nil
	}
	if (*u.AccountStatus == "64" || *u.AccountStatus == "97") && (*u.OriginalChargeOffAmt == 0) {
		*u.OriginalChargeOffAmt = *u.AmountPastDue
	}
	return nil
}

type GenerationCodeCheck struct {
	Surname *string
}

func (g *GenerationCodeCheck) Validate() error {
	if generationCodeRegExp.MatchString(*g.Surname) {
		return &common.RecordValidationError{
			ValidationType: "Generation Code Check",
			Value:          *g.Surname,
			ValidationErr:  "generation code found in above surname. please correct and try again.",
		}
	}
	return nil
}

type SSNCheck struct {
	SSN *common.NumberString
}

func (s *SSNCheck) Validate() error {
	if *s.SSN == "" || len(*s.SSN) != 9 {
		return &common.RecordValidationError{
			ValidationType: "SSN Check",
			Value:          *s.SSN,
			ValidationErr:  "SSN cannot be empty, and must be exactly 9 digits.",
		}
	}

	return nil
}
