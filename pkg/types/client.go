package types

import (
	"reflect"
	"strings"
)

var _ Client = (*client)(nil)

type Clients []Client

func (c Clients) FindClient(fileName string) Client {
	for _, client := range c {
		if strings.Contains(fileName, client.GetExpectedNameFormat()) {
			return client
		}
	}

	return nil
}

type GlobalState struct {
	Path                  string
	ShouldWriteExceptions bool
	OutputDirectory       string
	Filetype              string
}

type client struct {
	Name           string
	ReportName     string
	Equifax        string
	Experian       string
	Transunion     string
	Address        string
	PhoneNumber    string
	ExperianHeader string
}

func (c *client) GetExpectedNameFormat() string {
	var newName []string

	nameSplit := strings.Split(c.Name, " ")
	for i := range nameSplit {
		newName = append(newName, strings.ToUpper(nameSplit[i]))
	}

	return strings.Join(newName, "_")
}

func (c *client) GetEquifaxID() string {
	if c.Equifax != "" {
		return c.Equifax
	}

	return ""
}

func (c *client) GetTransunionID() string {
	if c.Transunion != "" {
		return c.Transunion
	}

	return ""

}

func (c *client) GetReportName() string {
	return c.ReportName
}

func (c *client) GetExperianHeader() string {
	return c.ExperianHeader
}

func (c *client) GetExperianID() string {
	return c.Experian
}

func (c *client) GetAddress() string {
	return c.Address
}

func (c *client) GetPhoneNumber() string {
	return c.PhoneNumber
}

func (c *client) GetCompanyName() string {
	return c.Name
}

func ClientFromSlice(contents []string) Client {
	newClient := &client{}

	value := reflect.Indirect(reflect.ValueOf(newClient))

	for i := range contents {
		value.Field(i).Set(reflect.ValueOf(contents[i]))
	}

	return newClient
}

func ParseClientSettings(clientSettingFileContents [][]string) Clients {
	var clients Clients
	for i := range clientSettingFileContents {
		if i > 0 {
			clients = append(clients, ClientFromSlice(clientSettingFileContents[i]))
		}
	}

	return clients
}
