package types

import (
	"data-furnishing-tooling/pkg/common"
	"github.com/moov-io/metro2/pkg/lib"
	"time"
)

type ReportingFile interface {
	ParseRecord(rowNum int, headers []string, record []string) []error
	Validate() common.ErrorAggregate
	GenerateSegments(client Client, bureau string) ([]lib.Record, error)
	GetFileName() string
	GetReportDate() time.Time
}

type ValidatorFn interface {
	Validate() error
}

type FileValidator interface {
	Validate() []error
}

type ReportingRecord interface {
	Validate() common.ErrorAggregate
	FromSlice(rowNumber int, header []string, record []string) []error
}

type Client interface {
	GetExpectedNameFormat() string
	GetEquifaxID() string
	GetTransunionID() string
	GetExperianID() string
	GetAddress() string
	GetPhoneNumber() string
	GetCompanyName() string
	GetReportName() string
	GetExperianHeader() string
}
