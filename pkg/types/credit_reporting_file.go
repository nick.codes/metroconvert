package types

import (
	"data-furnishing-tooling/pkg/common"
	"fmt"
	"github.com/moov-io/metro2/pkg/lib"
	"github.com/moov-io/metro2/pkg/utils"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var _ ReportingFile = (*CreditReportingFile)(nil)

type CreditReportingFile struct {
	FileName   string
	Records    []*CreditReportingRecord
	Validators []ValidatorFn
	ReportDate time.Time
}

func (c *CreditReportingFile) GetReportDate() time.Time {
	return c.ReportDate
}

func (c *CreditReportingFile) GetFileName() string {
	return c.FileName
}

func (c *CreditReportingFile) ParseRecord(rowNum int, headers []string, record []string) []error {
	var reportingRecord CreditReportingRecord

	err := reportingRecord.FromSlice(rowNum, headers, record)
	if err != nil {
		return err
	}

	c.Records = append(c.Records, &reportingRecord)
	return nil
}

func (c *CreditReportingFile) Validate() common.ErrorAggregate {
	errorsAgg := common.ErrorAggregate{
		ErrorsPrefix: fmt.Sprintf("errors encountered while validating %s:\n", c.FileName),
	}
	for _, record := range c.Records {
		err := record.Validate()
		if len(err.Errors) > 0 {
			errorsAgg.Errors = append(errorsAgg.Errors, err)
			continue
		}
	}

	return errorsAgg
}

func CreditReportingFileFromSlices(filePath string, records [][]string) (ReportingFile, error) {
	errAggregate := common.ErrorAggregate{
		ErrorsPrefix: fmt.Sprintf("errors encountered parsing file %s\n", filePath),
	}
	fileName := strings.Trim(filepath.Base(filePath), ".csv")
	fileNameSplit := strings.Split(fileName, "_")

	file := &CreditReportingFile{FileName: strings.Trim(filepath.Base(filePath), ".csv")}

	for i := range records {
		if i > 0 {
			err := file.ParseRecord(i, records[0], records[i])
			if err != nil {
				errAggregate.Errors = append(errAggregate.Errors, err...)
				continue
			}
		}
	}

	rptDate, _ := time.Parse("20060102", fileNameSplit[len(fileNameSplit)-1])
	file.ReportDate = rptDate

	if len(errAggregate.Errors) == 0 {
		return file, nil
	}

	return nil, errAggregate
}

func (c *CreditReportingFile) GenerateSegments(client Client, bureau string) ([]lib.Record, error) {
	var records []lib.Record
	var clientId string

	switch bureau {
	case "equifax":
		clientId = client.GetEquifaxID()
	case "transunion":
		clientId = client.GetTransunionID()
	default:
		clientId = client.GetExperianID()
	}

	if clientId == "" {
		return []lib.Record{}, nil
	}

	for i := range c.Records {
		record := c.Records[i]
		ssn, ssnConvErr := strconv.ParseInt(string(record.SocialSecurityNumber), 10, 64)
		if ssnConvErr != nil {
			return nil, ssnConvErr
		}
		baseSegment := lib.NewBaseSegment()
		baseRecord := baseSegment.(*lib.BaseSegment)
		baseRecord.RecordDescriptorWord = 0726
		baseRecord.IdentificationNumber = clientId
		baseRecord.ConsumerAccountNumber = record.ConsumerAccountNumber
		baseRecord.PaymentRating = record.PaymentRating
		baseRecord.PortfolioType = record.PortfolioType
		baseRecord.AccountType = record.AccountType
		baseRecord.DateOpened = utils.Time(record.DateOpen)
		baseRecord.TermsDuration = record.Terms
		baseRecord.TermsFrequency = record.TermsFrequency
		baseRecord.AccountStatus = record.AccountStatus
		baseRecord.PaymentHistoryProfile = "BBBBBBBBBBBBBBBBBBBBBBBB"
		baseRecord.DateAccountInformation = utils.Time(time.Now())
		baseRecord.Surname = record.LastName
		baseRecord.FirstName = record.FirstName
		baseRecord.MiddleName = record.MiddleName
		baseRecord.GenerationCode = record.GenerationCode
		baseRecord.DateBirth = utils.Time(record.DateOfBirth)
		baseRecord.SocialSecurityNumber = int(ssn)
		baseRecord.ECOACode = record.AssociationCode
		baseRecord.FirstLineAddress = record.Address1
		baseRecord.SecondLineAddress = record.Address2
		baseRecord.State = record.State
		baseRecord.ZipCode = record.ZipCode
		baseRecord.City = record.City
		records = append(records, baseSegment)
	}

	return records, nil
}
