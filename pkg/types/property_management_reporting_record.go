package types

import (
	"data-furnishing-tooling/pkg/common"
	"data-furnishing-tooling/pkg/validator"
	"fmt"
	"reflect"
	"time"
)

var _ ReportingRecord = (*PropertyManagementReportingRecord)(nil)

type PropertyManagementReportingRecord struct {
	PropertyManagementReportingData
	Validators []ValidatorFn
}

type PropertyManagementReportingData struct {
	TenantLastName                string
	TenantFirstName               string
	TenantMiddleName              string
	GenerationCode                string
	TenantSocialSecurityNumber    common.NumberString
	TenantDateOfBirth             time.Time
	TenantTelephoneNumber         common.NumberString
	FirstLineOfAddress            string
	SecondLineOfAddress           string
	City                          string
	State                         string
	ZipCode                       string
	RelationshipCode              string
	LeaseNumber                   string
	LeaseObligationStartDate      time.Time
	LeaseDuration                 string
	LeaseAmount                   common.Amount
	LeasePaymentAmountConfirmed   common.Amount
	LeaseStatus                   string
	LeaseCommentCode              string
	LeaseBalance                  common.Amount
	AmountPastDue                 common.Amount
	DateOfFirstDelinquency        time.Time
	DateClosed                    time.Time
	EffectiveDate                 time.Time
	PropertyManagementName        string
	PropertyManagementPhoneNumber common.NumberString
}

func (r *PropertyManagementReportingRecord) FromSlice(rowNumber int, header []string, record []string) []error {
	var data PropertyManagementReportingData
	var errAgg []error
	for i := 0; i < reflect.Indirect(reflect.ValueOf(data)).NumField(); i++ {
		fieldName := reflect.ValueOf(data).Type().Field(i).Name
		fieldType := reflect.Indirect(reflect.ValueOf(data)).Field(i).Type().String()
		headerName := header[i]
		parsedValue, err := dataCleaning(rowNumber, headerName, fieldType, fieldName, record[i])

		if err != nil {
			errAgg = append(errAgg, err)
			continue
		}

		reflect.ValueOf(&data).Elem().Field(i).Set(reflect.ValueOf(parsedValue))
	}

	r.PropertyManagementReportingData = data

	r.Validators = []ValidatorFn{
		&validator.ECOACheck{
			ECOACode:   &r.RelationshipCode,
			J2ECOACode: nil,
		},
		&validator.DateOpenCheck{
			DateOpen: &r.LeaseObligationStartDate,
		},
		&validator.UpdateChargeOffAmount{
			OriginalChargeOffAmt: nil,
			AccountStatus:        &r.LeaseStatus,
			AmountPastDue:        &r.AmountPastDue,
		},
		&validator.GenerationCodeCheck{
			Surname: &r.TenantLastName,
		},
		&validator.SSNCheck{
			SSN: &r.TenantSocialSecurityNumber,
		},
	}

	if len(errAgg) != 0 {
		return errAgg
	}
	return nil
}

func (r *PropertyManagementReportingRecord) Validate() common.ErrorAggregate {
	errorsAgg := common.ErrorAggregate{
		ErrorsPrefix: fmt.Sprintf("errors encountered while validating record with lease number # %s:\n", r.LeaseNumber),
	}

	for _, recordValidator := range r.Validators {
		err := recordValidator.Validate()
		if err != nil {
			errorsAgg.Errors = append(errorsAgg.Errors, err)
			continue
		}
	}

	return errorsAgg
}
