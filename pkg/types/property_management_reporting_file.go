package types

import (
	"data-furnishing-tooling/pkg/common"
	"fmt"
	"github.com/moov-io/metro2/pkg/lib"
	"github.com/moov-io/metro2/pkg/utils"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var _ ReportingFile = (*PropertyManagementReportingFile)(nil)

type PropertyManagementReportingFile struct {
	FileName   string
	Records    []*PropertyManagementReportingRecord
	Validators []ValidatorFn
	ReportDate time.Time
}

func (p *PropertyManagementReportingFile) GetReportDate() time.Time {
	return p.ReportDate
}

func (p *PropertyManagementReportingFile) GetFileName() string {
	return p.FileName
}

func (p *PropertyManagementReportingFile) ParseRecord(rowNum int, headers []string, record []string) []error {
	var reportingRecord PropertyManagementReportingRecord

	err := reportingRecord.FromSlice(rowNum, headers, record)
	if err != nil {
		return err
	}

	p.Records = append(p.Records, &reportingRecord)
	return nil
}

func (p *PropertyManagementReportingFile) Validate() common.ErrorAggregate {
	errorsAgg := common.ErrorAggregate{
		ErrorsPrefix: fmt.Sprintf("errors encountered while validating %s\n", p.FileName),
	}
	for _, record := range p.Records {
		err := record.Validate()
		if len(err.Errors) > 0 {
			errorsAgg.Errors = append(errorsAgg.Errors, err)
			continue
		}
	}

	return errorsAgg
}

func PropertyManagementReportingFileFromSlices(filePath string, records [][]string) (ReportingFile, error) {
	errAggregate := common.ErrorAggregate{
		ErrorsPrefix: fmt.Sprintf("errors encountered parsing file %s\n", filePath),
	}
	fileName := strings.Trim(filepath.Base(filePath), ".csv")
	fileNameSplit := strings.Split(fileName, "_")

	file := &PropertyManagementReportingFile{FileName: fileName}

	for i := range records {
		if i > 0 {
			err := file.ParseRecord(i, records[0], records[i])
			if err != nil {
				errAggregate.Errors = append(errAggregate.Errors, err...)
				continue
			}
		}
	}

	rptDate, _ := time.Parse("01022006", fileNameSplit[len(fileNameSplit)-1])
	file.ReportDate = rptDate
	return file, nil
}

func (p *PropertyManagementReportingFile) GenerateSegments(client Client, bureau string) ([]lib.Record, error) {
	var records []lib.Record
	var clientId string

	switch bureau {
	case "equifax":
		clientId = client.GetEquifaxID()
	case "transunion":
		clientId = client.GetTransunionID()
	default:
		clientId = client.GetExperianID()
	}

	if clientId == "" {
		return []lib.Record{}, nil
	}

	for i := range p.Records {
		record := p.Records[i]
		baseSegment := lib.NewBaseSegment()
		j1Seg := lib.NewJ1Segment()
		j2Seg := lib.NewJ2Segment()
		baseRecord := baseSegment.(*lib.BaseSegment)
		ssn, ssnConvErr := strconv.ParseInt(string(record.TenantSocialSecurityNumber), 10, 64)
		if ssnConvErr != nil {
			return nil, ssnConvErr
		}
		err := baseRecord.AddApplicableSegment(j1Seg)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
		err = baseRecord.AddApplicableSegment(j2Seg)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
		baseRecord.RecordDescriptorWord = 0726
		baseRecord.IdentificationNumber = clientId
		baseRecord.ConsumerAccountNumber = record.LeaseNumber
		baseRecord.PortfolioType = "O"
		baseRecord.AccountType = "29"
		baseRecord.DateOpened = utils.Time(record.LeaseObligationStartDate)
		baseRecord.TermsDuration = record.LeaseDuration
		baseRecord.TermsFrequency = "M"
		baseRecord.AccountStatus = record.LeaseStatus
		baseRecord.PaymentHistoryProfile = "BBBBBBBBBBBBBBBBBBBBBBBB"
		baseRecord.DateAccountInformation = utils.Time(time.Now())
		baseRecord.Surname = record.TenantLastName
		baseRecord.FirstName = record.TenantFirstName
		baseRecord.MiddleName = record.TenantMiddleName
		baseRecord.GenerationCode = record.GenerationCode
		baseRecord.DateBirth = utils.Time(record.TenantDateOfBirth)
		baseRecord.ECOACode = record.RelationshipCode
		baseRecord.FirstLineAddress = record.FirstLineOfAddress
		baseRecord.SecondLineAddress = record.SecondLineOfAddress
		baseRecord.SocialSecurityNumber = int(ssn)
		baseRecord.ZipCode = record.ZipCode
		baseRecord.City = record.City
		baseRecord.State = record.State
		records = append(records, baseSegment)
	}

	return records, nil
}
