package types

import (
	"data-furnishing-tooling/pkg/common"
	"errors"
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	CtxPathKey                  string = "fileSystemEntryPath"
	CtxOutputDirectoryPathKey   string = "outputDirectoryPath"
	CtxShouldWriteExceptionsKey string = "writeExceptions"
	CtxFileTypeKey              string = "fileType"
	TimeFormatMMDDYYYY          string = "1/2/2006"
	TimeFormatMMDDYY            string = "1/2/06"
	TimeFormatNoSlashMDDYYYY    string = "01022006"
)

func getTimeFormats() []string {
	return []string{
		TimeFormatMMDDYY,
		TimeFormatMMDDYYYY,
		TimeFormatNoSlashMDDYYYY,
	}
}

func dataCleaning(rowNumber int, headerName, fieldType, fieldName, dataItem string) (interface{}, error) {
	//Trim string and convert to all upper
	trimmedString := strings.Trim(strings.ToUpper(dataItem), " ")
	switch fieldType {
	case "common.NumberString":
		if dataItem == "" {
			return common.NumberString(""), nil
		}
		//Remove anything non-numeric
		cleanedStr := regexp.MustCompile("[^0-9]+").ReplaceAllString(trimmedString, "")
		return common.NumberString(cleanedStr), nil
	case "string":
		cleanedStr := regexp.MustCompile("[^a-zA-Z0-9 -]+").ReplaceAllString(trimmedString, "")
		return cleanedStr, nil
	case "time.Time":
		var parsedTime time.Time
		var err error
		formats := getTimeFormats()
		for j := range formats {
			timeString := dataItem
			if len(dataItem) < 8 {
				timeString = "0" + dataItem
			}
			parsedTime, err = time.Parse(formats[j], timeString)
			if err == nil {
				break
			}

			if err != nil && j == len(formats)-1 {
				parsedTime = time.Time{}
			}
		}
		return parsedTime, nil
	case "common.Amount":
		if dataItem == "" {
			return common.Amount(0), nil
		}
		//Remove money indicators, keep decimal
		stripped := regexp.MustCompile(`[$|,]`).ReplaceAllString(trimmedString, "")
		parsedFloat, err := strconv.ParseFloat(stripped, 64)
		if err != nil {
			return nil, &common.ParseError{
				StructField: fieldName,
				StructType:  fieldType,
				CSVHeader:   headerName,
				RowNumber:   rowNumber,
				CSVValue:    dataItem,
				ParseErr:    "expected a monetary value",
			}
		}
		amount := math.Round(parsedFloat)
		return common.Amount(amount), nil
	default:
		return nil, errors.New(fmt.Sprintf("unsupported field type found: %s", fieldType))
	}
}
