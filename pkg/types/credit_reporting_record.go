package types

import (
	"data-furnishing-tooling/pkg/common"
	"data-furnishing-tooling/pkg/validator"
	"fmt"
	"reflect"
	"time"
)

var _ ReportingRecord = (*CreditReportingRecord)(nil)

type CreditReportingData struct {
	AssociationCode                   string
	FirstName                         string
	MiddleName                        string
	LastName                          string
	GenerationCode                    string
	Address1                          string
	Address2                          string
	City                              string
	State                             string
	ZipCode                           string
	SocialSecurityNumber              common.NumberString
	PhoneNumber                       common.NumberString
	DateOfBirth                       time.Time
	ConsumerInformationIndicator      string
	JointAssociationCode              string
	JointFirstName                    string
	JointMiddleName                   string
	JointLastName                     string
	JointAddress1                     string
	JointAddress2                     string
	JointCity                         string
	JointState                        string
	JointZipCode                      string
	JointSocialSecurityNumber         common.NumberString
	JointPhoneNumber                  common.NumberString
	JointDateOfBirth                  time.Time
	JointConsumerInformationIndicator string
	ConsumerAccountNumber             string
	PortfolioType                     string
	AccountType                       string
	DateOpen                          time.Time
	DateOfFirstDelinquency            time.Time
	DateOfLastPayment                 time.Time
	DateClosed                        time.Time
	AccountStatus                     string
	PaymentRating                     string
	SpecialCommentCode                string
	ComplianceConditionCode           string
	CreditLimit                       common.Amount
	HighestCredit                     common.Amount
	CurrentBalance                    common.Amount
	AmountPastDue                     common.Amount
	MonthlyPayment                    common.Amount
	ActualPayment                     common.Amount
	TermsFrequency                    string
	Terms                             string
	OriginalChargeOffAmount           common.Amount
	DateOfAccountInformation          time.Time
}

type CreditReportingRecord struct {
	CreditReportingData
	Validators []ValidatorFn
}

func (c *CreditReportingRecord) FromSlice(rowNumber int, header []string, record []string) []error {
	var data CreditReportingData
	var errors []error
	for i := 0; i < reflect.Indirect(reflect.ValueOf(data)).NumField(); i++ {
		fieldName := reflect.ValueOf(data).Type().Field(i).Name
		fieldType := reflect.Indirect(reflect.ValueOf(data)).Field(i).Type().String()
		headerName := header[i]
		parsedValue, err := dataCleaning(rowNumber, headerName, fieldType, fieldName, record[i])

		if err != nil {
			errors = append(errors, err)
			continue
		}

		reflect.ValueOf(&data).Elem().Field(i).Set(reflect.ValueOf(parsedValue))
	}

	c.CreditReportingData = data

	c.Validators = []ValidatorFn{
		&validator.ECOACheck{
			ECOACode:   &c.AssociationCode,
			J2ECOACode: nil,
		},
		&validator.DateOpenCheck{
			DateOpen: &c.DateOpen,
		},
		&validator.UpdateChargeOffAmount{
			OriginalChargeOffAmt: nil,
			AccountStatus:        &c.AccountStatus,
			AmountPastDue:        &c.AmountPastDue,
		},
		&validator.GenerationCodeCheck{
			Surname: &c.LastName,
		},
		&validator.SSNCheck{
			SSN: &c.SocialSecurityNumber,
		},
	}

	if len(errors) != 0 {
		return errors
	}
	return nil
}

func (c *CreditReportingRecord) Validate() common.ErrorAggregate {
	errorsAgg := common.ErrorAggregate{
		ErrorsPrefix: fmt.Sprintf("errors encountered while validating record with  account # %s:\n", c.ConsumerAccountNumber),
	}

	for _, recordValidator := range c.Validators {
		err := recordValidator.Validate()
		if err != nil {
			errorsAgg.Errors = append(errorsAgg.Errors, err)
			continue
		}
	}

	return errorsAgg
}
