package common

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type ErrorAggregate struct {
	ErrorsPrefix string
	Errors       []error
}

type ValidationError struct {
	RowNumber string
	Err       error
}

func (v *ValidationError) Error() string {
	return fmt.Sprintf("error occurred while validating data for record #%s: %s\n", v.RowNumber, v.Err)
}

func (e ErrorAggregate) Error() string {
	var joinedString string
	for i := range e.Errors {
		errorItem := e.Errors[i]

		joinedString += errorItem.Error()
	}

	return e.ErrorsPrefix + joinedString
}

type ParseError struct {
	StructField string `json:"record_field"`
	StructType  string `json:"record_field_type"`
	CSVHeader   string `json:"csv_column_name"`
	CSVValue    string `json:"csv_column_value"`
	RowNumber   int    `json:"row_number"`
	ParseErr    string `json:"error"`
}

func (p *ParseError) Error() string {
	jsonBytes, err := json.Marshal(&p)
	if err != nil {
		return ""
	}

	var dst bytes.Buffer
	indentErr := json.Indent(&dst, jsonBytes, "", "  ")

	if indentErr != nil {
		return ""
	}

	return fmt.Sprintf("%s\n", dst.String())
}

type RecordValidationError struct {
	ValidationType string      `json:"validation_type"`
	Value          interface{} `json:"value"`
	ValidationErr  string      `json:"error"`
}

func (r *RecordValidationError) Error() string {
	jsonBytes, err := json.Marshal(&r)
	if err != nil {
		return ""
	}

	var dst bytes.Buffer
	indentErr := json.Indent(&dst, jsonBytes, "  ", "  ")

	if indentErr != nil {
		return ""
	}

	return fmt.Sprintf("%s,\n", dst.String())
}
