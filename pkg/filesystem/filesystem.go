package filesystem

import (
	"bytes"
	"context"
	"data-furnishing-tooling/pkg/types"
	"encoding/csv"
	"fmt"
	metro2 "github.com/moov-io/metro2/pkg/file"
	"github.com/sirupsen/logrus"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func ProcessFileSystemEntry(ctx context.Context, logger *logrus.Logger) error {
	var clients types.Clients
	var fsys fs.FS
	var clientSettingsPath fs.FS
	var processedCount int
	var errArr []error
	// Load global values from context
	globalState, err := loadGlobalStateFromContext(ctx)

	if filepath.Ext(globalState.Path) == "" {
		clientSettingsPath = os.DirFS(globalState.Path)
	} else {
		clientSettingsPath = os.DirFS(filepath.Dir(globalState.Path))
	}

	// Load settings context on whether path provided is a single file or directory
	fileContents, err := loadClientSettingsFile(clientSettingsPath)
	if err != nil {
		return fmt.Errorf("error loading client settings: %s", err)
	}

	// Load clients from client-settings.csv

	reader := csv.NewReader(bytes.NewReader(fileContents))
	rows, err := reader.ReadAll()

	if err != nil {
		return fmt.Errorf("error reading rows from `client-settings.csv`: %s", err)
	}

	clients = types.ParseClientSettings(rows)

	logger.Debugf("clients created: %#v", clients)

	// Process single file
	absPath, err := filepath.Abs(globalState.Path)
	if err != nil {
		return fmt.Errorf("error converting path %s to absolute path: %s", globalState.Path, err)
	}

	if filepath.Ext(absPath) == ".csv" {
		processedCount += 1
		contents, err := os.ReadFile(absPath)
		if err != nil {
			return fmt.Errorf("cannot read file at path %s: %s", absPath, err)
		}

		logger.Debugf("processing file %s", absPath)
		fileProcessErr := processFile(globalState, contents, absPath, logger, clients)
		if fileProcessErr != nil {
			return err
		}

	} else if filepath.Ext(globalState.Path) != "" {
		return fmt.Errorf("expected file to be of type '.csv', got: %s", filepath.Ext(globalState.Path))
	} else {

		// Clean directory and get count of files, less client-settings.csv
		err := cleanDirectory(globalState, logger)

		if err != nil {
			return fmt.Errorf("encountered error when cleaning directory of old files and counting csvs to process: %s", err)
		}

		fsys = os.DirFS(globalState.Path)

		walkErr := fs.WalkDir(fsys, ".", func(path string, d fs.DirEntry, err error) error {
			absPath, err := filepath.Abs(ctx.Value(types.CtxPathKey).(string) + "/" + path)

			if err != nil {
				return fmt.Errorf("unable to get absolute path to file %s: %s", path, err)
			}

			// for every csv file encountered, process the file contents individually.
			if filepath.Ext(d.Name()) == ".csv" && d.Name() != "client-settings.csv" {
				processedCount += 1
				contents, err := fs.ReadFile(fsys, path)
				if err != nil {
					return fmt.Errorf("cannot read file %s: %s", absPath, err)
				}
				fileProcessErr := processFile(globalState, contents, absPath, logger, clients)
				if fileProcessErr != nil {
					errArr = append(errArr, fileProcessErr)
				}
			}
			return nil
		})

		if walkErr != nil {
			return fmt.Errorf("error occured walking directory %s: %s", globalState.Path, walkErr)
		}
	}

	if len(errArr) > 0 {
		logger.Info("errors reported above. Some files were not processed successfully. If write exceptions was included, these errors will be in the applicable exception file.")
	}

	logger.Debugf("file count seen: %d, fileCount Successfully Processed: %d\n", processedCount, processedCount-len(errArr))
	return nil
}

func cleanDirectory(globalState *types.GlobalState, logger *logrus.Logger) error {
	// Walk dir, deleting old exceptions files and old metro2 files.
	fsys := os.DirFS(globalState.Path)
	err := fs.WalkDir(fsys, ".", func(path string, d fs.DirEntry, err error) error {
		absPath, err := filepath.Abs(globalState.Path + "/" + path)
		if err != nil {
			return fmt.Errorf("error converting path %s to absolute path: %s", globalState.Path, err)
		}
		// Clean directory of exceptions and all old metro2 files.
		if strings.Contains(absPath, "exceptions_") || (strings.Contains(absPath, "output/") && !d.IsDir()) {
			err := os.Remove(absPath)
			if err != nil {
				logger.Errorf("error removing old exception files from directory: %s", err)
				return err
			}
		}

		return nil
	})

	if err != nil {
		return err
	}

	return nil
}

func processFile(globalState *types.GlobalState, contents []byte, absPath string, logger *logrus.Logger, clients types.Clients) error {
	var reportingFile types.ReportingFile

	log := logger.WithField("file", absPath)
	fileName := filepath.Base(absPath)
	log.Infof("processing file %s", fileName)
	fileNameLessExtension := strings.Trim(filepath.Base(absPath), filepath.Ext(absPath))
	log.Debugf("base file name created: %s", fileNameLessExtension)
	inputFolder := filepath.Dir(absPath)
	log.Debugf("input folder for client: %s", inputFolder)

	client := clients.FindClient(absPath)

	log.Debugf("client found for %s: %#v", absPath, client)

	reportingFile, parsingErr := parseReportingFile(globalState, absPath, contents, log)

	// return early if reporting file does not parse properly.
	if parsingErr != nil {
		if globalState.ShouldWriteExceptions {
			writeExceptions(inputFolder, fileNameLessExtension, log, parsingErr)
		}
		log.Error(parsingErr)
		return parsingErr
	}

	log.Debugf("File successfully created: %#v", reportingFile)

	validationErr := reportingFile.Validate()

	//WriteExceptions
	if len(validationErr.Errors) != 0 {
		if globalState.ShouldWriteExceptions {
			writeExceptions(inputFolder, fileNameLessExtension, log, validationErr)
		}
		log.Error(validationErr)
		return validationErr
	}

	bureaus := []string{"equifax", "transunion", "experian"}

	for _, bureau := range bureaus {
		err := generateMetro2File(globalState, inputFolder, client, reportingFile, bureau)
		if err != nil {
			log.Error(fmt.Sprintf("error encountered generating metro2 file for %s for client %s: %s", bureau, client.GetCompanyName(), err))
			writeExceptions(inputFolder, fileNameLessExtension, log, err)
		}
	}

	return nil
}

func parseReportingFile(globalState *types.GlobalState, absPath string, content []byte, log *logrus.Entry) (types.ReportingFile, error) {
	reader := bytes.NewReader(content)
	csvReader := csv.NewReader(reader)
	slices, err := csvReader.ReadAll()
	if err != nil {
		log.Error(err)
		return nil, err
	}

	switch globalState.Filetype {
	case "general":
		return types.CreditReportingFileFromSlices(absPath, slices)
	case "rental":
		return types.PropertyManagementReportingFileFromSlices(absPath, slices)
	default:
		return parseReportingFileAndDetectType(absPath, content, log)
	}
}

func parseReportingFileAndDetectType(absPath string, reportingFileContents []byte, logger *logrus.Entry) (types.ReportingFile, error) {
	logger.Debug("Reading file contents...")
	reader := csv.NewReader(bytes.NewReader(reportingFileContents))

	recordSlices, err := reader.ReadAll()

	if err != nil {
		return nil, fmt.Errorf("error reading file contents: %s", err)
	}

	if strings.Contains(absPath, "RENTAL_") {
		logger.Debug("Creating Rental File")
		return types.PropertyManagementReportingFileFromSlices(absPath, recordSlices)
	}
	logger.Debug("Creating General File")
	return types.CreditReportingFileFromSlices(absPath, recordSlices)
}

func writeExceptions(folder string, fileName string, entry *logrus.Entry, err error) {
	f, createErr := os.OpenFile(filepath.Join(folder, strings.Join([]string{"exceptions_", fileName, ".txt"}, "")), os.O_CREATE|os.O_APPEND|os.O_RDWR, 0777)

	if createErr != nil {
		entry.Error(createErr)
		return
	}

	_, writeErr := f.WriteString(err.Error())

	if writeErr != nil {
		entry.Error(writeErr)
		return
	}
}

func generateMetro2File(state *types.GlobalState, inputFolder string, client types.Client, file types.ReportingFile, bureau string) error {
	var bureauPrefix string
	outputDir := filepath.Join(inputFolder, "output")
	typePrefix := "GN"
	switch bureau {
	case "equifax":
		bureauPrefix = "EQ"
	case "transunion":
		bureauPrefix = "TU"
	case "experian":
		bureauPrefix = ""
	}

	if strings.Contains(file.GetFileName(), "RENTAL_") {
		typePrefix = "RT"
	}
	metro2File, err := metro2.NewFile("character")
	if err != nil {
		return err
	}

	segmentRecords, err := file.GenerateSegments(client, bureau)

	if len(segmentRecords) == 0 {
		return nil
	}

	if err != nil {
		return err
	}
	for i := range segmentRecords {
		addSegmentErr := metro2File.AddDataRecord(segmentRecords[i])
		if addSegmentErr != nil {
			return addSegmentErr
		}
	}

	headerRecord := generateHeaderRecord(client, bureau, time.Now())

	addHeaderErr := metro2File.SetRecord(headerRecord)

	if addHeaderErr != nil {
		return fmt.Errorf("addHeaderError: %s", addHeaderErr)
	}

	trailer, _ := metro2File.GeneratorTrailer()

	addTrailerErr := metro2File.SetRecord(trailer)

	if addTrailerErr != nil {
		return fmt.Errorf("addTrailerErr: %s", addTrailerErr)
	}

	absPath, err := filepath.Abs(state.OutputDirectory)

	if err == nil {
		outputDir = absPath
	}

	mkDirErr := os.MkdirAll(outputDir, 0777)
	logrus.StandardLogger().Debug(fmt.Sprintf("outputDir: %s", outputDir))
	if mkDirErr != nil {
		return fmt.Errorf("mkdir err: %s", err)
	}
	fileName := generateFileName(bureauPrefix, typePrefix, client, file)
	newFile, createFileErr := os.Create(filepath.Join(outputDir, fileName))
	if createFileErr != nil {
		return fmt.Errorf("create file err: %s", createFileErr)
	}

	_, writeErr := newFile.Write([]byte(metro2File.String(true)))

	if writeErr != nil {
		return fmt.Errorf("write file err: %s", writeErr)
	}
	return nil
}

func generateFileName(bureauPrefix string, typePrefix string, client types.Client, file types.ReportingFile) string {
	//Default file name format for bureaus other than experian, addition of client name for separation
	fileFormat := "%s_%s_%s_%s_%s.txt"
	clientName := strings.Replace(strings.ToUpper(client.GetCompanyName()), " ", "_", -1)
	reportDate := file.GetReportDate()

	if bureauPrefix != "" {
		return fmt.Sprintf(fileFormat, bureauPrefix, typePrefix, clientName, reportDate.Format("20060102"), time.Now().Format("20060102"))
	}

	return fmt.Sprintf("%s.%s.%s.%s.%s.txt", client.GetExperianHeader(), typePrefix, clientName, reportDate.Format("20060102"), reportDate.Format("20060102"))
}
