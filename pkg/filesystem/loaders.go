package filesystem

import (
	"context"
	"data-furnishing-tooling/pkg/types"
	"fmt"
	"io/fs"
)

func loadClientSettingsFile(fileSystem fs.FS) ([]byte, error) {
	dir, dirReadErr := fs.ReadDir(fileSystem, ".")
	if dirReadErr != nil {
		return nil, fmt.Errorf("error reading directory %s: %s", fileSystem, dirReadErr)
	}

	for _, file := range dir {
		if file.Name() == "client-settings.csv" {
			return fs.ReadFile(fileSystem, file.Name())
		}
	}

	return nil, fs.ErrNotExist
}

func loadGlobalStateFromContext(ctx context.Context) (*types.GlobalState, error) {
	fsPath, ok := ctx.Value(types.CtxPathKey).(string)
	if !ok {
		return nil, fmt.Errorf("path %s not successfully cast to string", ctx.Value(types.CtxPathKey))
	}

	shouldWriteExceptions, ok := ctx.Value(types.CtxShouldWriteExceptionsKey).(bool)
	if !ok {
		return nil, fmt.Errorf("writeExceptions %#v not successfully cast to bool", ctx.Value(types.CtxShouldWriteExceptionsKey))
	}

	outputDirectory, ok := ctx.Value(types.CtxOutputDirectoryPathKey).(string)
	if !ok {
		return nil, fmt.Errorf("output directory path %s not successfully cast to string", ctx.Value(types.CtxOutputDirectoryPathKey))
	}

	fileType, ok := ctx.Value(types.CtxFileTypeKey).(string)
	if !ok {
		return nil, fmt.Errorf("file type %s not successfully cast to string", ctx.Value(types.CtxFileTypeKey))
	}

	return &types.GlobalState{
		Path:                  fsPath,
		ShouldWriteExceptions: shouldWriteExceptions,
		OutputDirectory:       outputDirectory,
		Filetype:              fileType,
	}, nil
}
