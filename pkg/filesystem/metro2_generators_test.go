package filesystem_test

import (
	"data-furnishing-tooling/pkg/filesystem"
	"data-furnishing-tooling/pkg/mocks"
	"github.com/moov-io/metro2/pkg/lib"
	"github.com/moov-io/metro2/pkg/utils"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"testing"
	"time"
)

func Test_GenerateHeaderRecord_HappyPath(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockClient := mocks.NewMockClient(ctrl)
	mockClient.EXPECT().GetCompanyName().Return("Mock Company")
	mockClient.EXPECT().GetAddress().Return("42 Wallaby Way")
	mockClient.EXPECT().GetPhoneNumber().Return("8435501234")
	mockClient.EXPECT().GetExperianID().Return("EXPMOCK123")
	mockClient.EXPECT().GetTransunionID().Return("TUMOCK456")
	mockClient.EXPECT().GetEquifaxID().Return("EFXMOCK789")

	nowtime := time.Now()

	testCases := []struct {
		bureau   string
		expected lib.HeaderRecord
	}{
		{
			bureau: "experian",
			expected: lib.HeaderRecord{
				RecordDescriptorWord:      0726,
				RecordIdentifier:          lib.HeaderIdentifier,
				ExperianProgramIdentifier: "EXPMOCK123",
				ActivityDate:              utils.Time(nowtime),
				DateCreated:               utils.Time(nowtime),
				ProgramDate:               utils.Time{},
				ProgramRevisionDate:       utils.Time{},
				ReporterName:              "Mock Company",
				ReporterAddress:           "42 Wallaby Way",
				ReporterTelephoneNumber:   int64(8435501234),
			},
		},
		{
			bureau: "transunion",
			expected: lib.HeaderRecord{
				RecordDescriptorWord:        0726,
				RecordIdentifier:            lib.HeaderIdentifier,
				TransUnionProgramIdentifier: "TUMOCK456",
				ActivityDate:                utils.Time(nowtime),
				DateCreated:                 utils.Time(nowtime),
				ReporterName:                "DATALINX",
				ReporterAddress:             "22722 29th Drive SE, Suite 100, Bothell, WA  98021",
				ReporterTelephoneNumber:     int64(4257804530),
			},
		},
		{
			bureau: "equifax",
			expected: lib.HeaderRecord{
				RecordDescriptorWord:     0726,
				RecordIdentifier:         lib.HeaderIdentifier,
				EquifaxProgramIdentifier: "EFXMOCK789",
				ActivityDate:             utils.Time(nowtime),
				DateCreated:              utils.Time(nowtime),
				ReporterName:             "DATALINX",
				ReporterAddress:          "22722 29th Drive SE, Suite 100, Bothell, WA  98021",
				ReporterTelephoneNumber:  int64(4257804530),
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.bureau, func(t *testing.T) {
			result := filesystem.GenerateHeaderRecord(mockClient, tc.bureau, nowtime)
			assert.Equal(t, tc.expected, *result)
		})

	}
}
