package filesystem

import (
	"data-furnishing-tooling/pkg/types"
	"github.com/moov-io/metro2/pkg/lib"
	"github.com/moov-io/metro2/pkg/utils"
	"strconv"
	"strings"
	"time"
)

func generateHeaderRecord(client types.Client, bureau string, timeObject time.Time) *lib.HeaderRecord {
	equifaxId := ""
	experianId := ""
	transunionId := ""
	reporterName := "DATALINX LLC"
	reporterAddress := "22722 29th Drive SE, Suite 100, Bothell, WA  98021"
	reporterTelephone := int64(4257804530)

	switch bureau {
	case "experian":
		reporterName = strings.ToUpper(client.GetReportName())
		reporterAddress = client.GetAddress()
		reporterTelephone, _ = strconv.ParseInt(client.GetPhoneNumber(), 10, 64)
		experianId = client.GetExperianID()
	case "transunion":
		transunionId = "DATALNXRNT"
		reporterName = "DATALINX"
	case "equifax":
		equifaxId = "V000051244"
	}

	return &lib.HeaderRecord{
		RecordDescriptorWord:        0726,
		RecordIdentifier:            lib.HeaderIdentifier,
		EquifaxProgramIdentifier:    equifaxId,
		ExperianProgramIdentifier:   experianId,
		TransUnionProgramIdentifier: transunionId,
		ActivityDate:                utils.Time(timeObject),
		DateCreated:                 utils.Time(timeObject),
		ReporterName:                reporterName,
		ReporterAddress:             reporterAddress,
		ReporterTelephoneNumber:     reporterTelephone,
	}
}
