package filesystem_test

import (
	"context"
	"data-furnishing-tooling/pkg/filesystem"
	"data-furnishing-tooling/pkg/types"
	"errors"
	"github.com/stretchr/testify/assert"
	"io/fs"
	"testing"
	"testing/fstest"
)

type StubFailingFS struct {
}

func (s StubFailingFS) Open(name string) (fs.File, error) {
	return nil, errors.New("oh no, i always fail")
}

func TestLoadClientSettingsFile_HappyPath(t *testing.T) {
	fileSystem := fstest.MapFS{
		"client-settings.csv": &fstest.MapFile{
			Data: []byte("key,value\nname,Test Client\nid,12345"),
		},
	}

	fileContents, err := filesystem.LoadClientSettingsFile(fileSystem)
	assert.NoError(t, err)
	assert.Equal(t, []byte("key,value\nname,Test Client\nid,12345"), fileContents)
}

func TestLoadClientSettingsFile_DirLoadErrPath(t *testing.T) {
	fileContents, err := filesystem.LoadClientSettingsFile(StubFailingFS{})
	assert.Error(t, err)
	assert.ErrorContains(t, err, "error reading directory")
	assert.Nil(t, fileContents)
}

func TestLoadClientSettingsFile_FileNotFoundErrPath(t *testing.T) {
	fileSystem := fstest.MapFS{
		"client-settings2.csv": &fstest.MapFile{
			Data: []byte("key,value\nname,Test Client\nid,12345"),
		},
	}

	fileContents, err := filesystem.LoadClientSettingsFile(fileSystem)
	assert.Error(t, err)
	assert.ErrorIs(t, err, fs.ErrNotExist)
	assert.Nil(t, fileContents)
}

func TestLoadGlobaStateFromContext_HappyPath(t *testing.T) {
	mockContext := context.Background()
	mockContext = context.WithValue(mockContext, types.CtxPathKey, "MOCK PATH")
	mockContext = context.WithValue(mockContext, types.CtxShouldWriteExceptionsKey, true)
	mockContext = context.WithValue(mockContext, types.CtxOutputDirectoryPathKey, "output")
	mockContext = context.WithValue(mockContext, types.CtxFileTypeKey, "all")

	globalState, err := filesystem.LoadGlobalStateFromContext(mockContext)
	assert.Nil(t, err)
	assert.Equal(t, "MOCK PATH", globalState.Path)
	assert.Equal(t, true, globalState.ShouldWriteExceptions)
	assert.Equal(t, "output", globalState.OutputDirectory)
	assert.Equal(t, "all", globalState.Filetype)
}
