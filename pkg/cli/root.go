package cli

import (
	"context"
	"data-furnishing-tooling/pkg/filesystem"
	"data-furnishing-tooling/pkg/types"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
	"path/filepath"
)

func NewRoot() *cobra.Command {
	var verboseMode bool
	var outputDirectory string
	var shouldWriteExceptions bool
	cmd := &cobra.Command{
		Use:   "metroconvert {file|directory} <filetype>",
		Short: "A file converter for metro2 files",
		Long: `A converter tool purpose-built to generate metro2 files 
			from specified file specifications.`,
		PreRunE: func(cmd *cobra.Command, args []string) error {
			if !(len(args) > 0) {
				return fmt.Errorf("commands are required to run this tool")
			}
			// Get absolute path to file system entry and set in context.
			absPath, _ := filepath.Abs(args[0])
			ctx := context.WithValue(cmd.Context(), types.CtxPathKey, absPath)
			cmd.SetContext(ctx)

			// Validate file system entry exists, and return error if not.
			// File info is not needed for the way we are parsing the file system.
			_, err := os.Stat(absPath)

			if err != nil {
				return fmt.Errorf("unable to find file or directory %s, please verify this is a valid file and try again", args[0])
			}

			return nil
		},
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) > 2 {
				return fmt.Errorf("requires two or less arguments")
			}

			// Second arg is optional, no need to validate if not there.
			if len(args) == 2 {
				availableOptions := []string{"rental", "general"}

				for i := range availableOptions {
					if args[1] == availableOptions[i] {
						return nil
					}
				}

				return fmt.Errorf("filetype must be one of rental or general")
			}

			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
			filetype := "all"
			if len(args) == 2 {
				filetype = args[1]
			}
			ctx := context.WithValue(cmd.Context(), types.CtxOutputDirectoryPathKey, outputDirectory)
			ctx2 := context.WithValue(ctx, types.CtxShouldWriteExceptionsKey, shouldWriteExceptions)
			ctx3 := context.WithValue(ctx2, types.CtxFileTypeKey, filetype)
			cmd.SetContext(ctx3)
			logger := logrus.StandardLogger()
			if verboseMode {
				logger.SetLevel(logrus.DebugLevel)
			} else {
				logger.SetLevel(logrus.InfoLevel)
			}

			err := filesystem.ProcessFileSystemEntry(cmd.Context(), logger)
			if err != nil {
				logger.Errorf("%s", err)
				os.Exit(2)
			}

			os.Exit(0)
		},
	}

	cmd.PersistentFlags().BoolVarP(&verboseMode, "verbose", "v", false, "optional, runs metroconvert in debug mode. Default: false")
	cmd.PersistentFlags().StringVarP(&outputDirectory, "output", "o", "", "optional, path to the directory to write metro2 files to. Default: output folder of client")
	cmd.PersistentFlags().BoolVarP(&shouldWriteExceptions, "write-exceptions", "w", false, "optional, should write exceptions to file, Default: false")
	return cmd
}
